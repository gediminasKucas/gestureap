package dk.itu.gallery;

import android.content.Context;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

/**
 * Created by Geiminas on 5/23/2016.
 */
public class Clasificator {
    public static final String NONE = "none";
    Instances data;
    Instance runningInstance;
    J48 tree;
    int counter = 0;

    public Clasificator(Context ctx) {
        try{
            CSVLoader loader = new CSVLoader();
            loader.setSource(ctx.getAssets().open("test_gesture.csv"));
            data = loader.getDataSet();
            runningInstance = new DenseInstance(data.numAttributes());
            runningInstance.setDataset(data);

            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(0);

            String[] options = new String[1];
            options[0] = "-U";            // unpruned tree
            tree = new J48();         // new instance of tree
            tree.setOptions(options);     // set the options
            tree.buildClassifier(data);   // build classifier

//            Evaluation eval = new Evaluation(data);
//            eval.evaluateModel(tree, data);
//            System.out.println(eval.toSummaryString("\nResults\n======\n", false));
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    public String putRunningInstance(double[] values){
        if(values.length != 6)
            return NONE;
        for (int i = 0; i < values.length; i++) {
            runningInstance.setValue(counter*6+i,values[i]);
        }
        if(counter == 29){
            counter = counter - 2;
            double val = 0;
            try {
                val = tree.classifyInstance(runningInstance);
            } catch (Exception e) {
                e.printStackTrace();
                return NONE;
            }
            String gesture = data.classAttribute().value((int) val);
            System.out.println(gesture);
            return gesture;
        }else
            counter++;
        return NONE;
    }
}
