package dk.itu.gallery;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    static final int PICK_DEVICE_REQUEST = 1;

    ViewPager viewPager;
    CustomSwipeAdapter adapter;
    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice mDevice;
    ConnectedThread connectedThread;
    ConnectThread connectThread;
    Clasificator clasificator;

    Button discButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothAdapter	= BluetoothAdapter.getDefaultAdapter();

        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new CustomSwipeAdapter(this);
        viewPager.setAdapter(adapter);

        discButton = (Button) findViewById(R.id.dic_button);
        discButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                startActivityForResult(new Intent(MainActivity.this, Pop.class), PICK_DEVICE_REQUEST, null);
                startActivity(new Intent(MainActivity.this, Pop.class));
            }
        });
        clasificator = new Clasificator(this);
    }

    private void showToast(String text){
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PICK_DEVICE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                BluetoothDevice device = (BluetoothDevice)data.getParcelableExtra(Pop.DEVICE_LABEL);
                showToast(device.getName());

                if(connectThread != null) connectThread.cancel();
                connectThread = new ConnectThread(device);
                connectThread.start();
            }
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String gesture = (String) msg.obj;
            int begin = (int)msg.arg1;
            int end = (int)msg.arg2;

            switch(msg.what) {
                case 1:
                    showToast(gesture);
                    switch (gesture) {
                        case "up":
                        case "down":
                        case "left":
                            viewPager.arrowScroll(View.FOCUS_LEFT);
                        case "right":
                            viewPager.arrowScroll(View.FOCUS_RIGHT);
                    }
            }
        }
    };

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            mmDevice = device;
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) { }
            mmSocket = tmp;
        }
        public void run() {
            bluetoothAdapter.cancelDiscovery();
            try {
                //mmSocket.connect();
                if(connectedThread != null)connectedThread.cancel();
                connectedThread = new ConnectedThread(mmSocket);
                connectedThread.start();

            } catch (Exception connectException) {
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }
        }
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private boolean running = true;
        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
//            try {
//                tmpIn = socket.getInputStream();
//                tmpOut = socket.getOutputStream();
//            } catch (IOException e) { }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }
        public void run() {
            byte[] buffer = new byte[1024];
            int begin = 0;
            int bytes = 0;
            while (running) {
                try {
//                    bytes += mmInStream.read(buffer, bytes, buffer.length - bytes);
//                    for(int i = begin; i < bytes; i++) {
//                        if(buffer[i] == "#".getBytes()[0]) {
//                            mHandler.obtainMessage(1, begin, i, buffer).sendToTarget();
//                            begin = i + 1;
//                            if(i == bytes - 1) {
//                                bytes = 0;
//                                begin = 0;
//                            }
//                        }
//                    }
                    String gesture = clasificator.putRunningInstance(new double[]{256, 256, 25, 5, 5, 6});
                    if(gesture != Clasificator.NONE)
                        mHandler.obtainMessage(1, gesture).sendToTarget();
                    this.sleep(5000);
                } catch (Exception e) {
                    cancel();
                }
            }
        }
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
            finally {
                running = false;
            }
        }
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
            finally {
                running = false;
            }
        }
    }

}
