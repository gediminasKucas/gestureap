package dk.itu.gallery;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Geiminas on 5/23/2016.
 */
public class Pop extends Activity {
    public static final String DEVICE_LABEL = "DEVICE";
    ListView listView;
    CustomArrayAdapter mAdapter;
    BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pop);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width * 0.8), (int)(height * 0.5));

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BluetoothDevice device = mAdapter.getItem(position);
                Intent resultIntent = new Intent();
                resultIntent.putExtra(DEVICE_LABEL, device);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });
        bluetoothAdapter	= BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) bluetoothAdapter.enable();
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        mAdapter = new CustomArrayAdapter(this,
                R.layout.device_view, new ArrayList(pairedDevices));
        listView.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
