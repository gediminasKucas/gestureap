package dk.itu.gallery;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.FutureTask;

/**
 * Created by Geiminas on 5/23/2016.
 */
public class CustomArrayAdapter extends ArrayAdapter<BluetoothDevice> {

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        BluetoothDevice device = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.device_view, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView address = (TextView) convertView.findViewById(R.id.address);
        // Populate the data into the template view using the data object
        name.setText(device.getName());
        address.setText(device.getAddress());
        // Return the completed view to render on screen

        return convertView;
    }


    public CustomArrayAdapter(Context context, int resource) {
        super(context, resource);
    }


    public CustomArrayAdapter(Pop pop, int device_view, ArrayList arrayList) {
        super(pop, device_view, arrayList);
    }
}
